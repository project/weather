<?php

namespace Drupal\Tests\weather\Functional;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\weather\Entity\WeatherPlace;
use Drupal\Tests\BrowserTestBase;
use Drupal\weather\Service\HelperService;

/**
 * Tests functions of weather.module.
 *
 * @group Weather
 */
class FunctionsTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['weather'];

  /**
   * The tests don't need markup, so use 'stark' as theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test HelperService::getLinkForGeoid().
   */
  public function testFunctionWeatherGetLinkForGeoId() {
    $entityWeatherMock = $this->getMockBuilder(WeatherPlace::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entityWeatherMock->expects($this->any())
      ->method('country')
      ->will($this->returnValue("Germany"));

    $entityStorage = $this->getMockBuilder(EntityStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entityStorage->expects($this->any())
      ->method('load')
      ->willReturn($entityWeatherMock);

    $entityTypeManager = $this->getMockBuilder(
      EntityTypeManagerInterface::class
    )
      ->disableOriginalConstructor()
      ->getMock();
    $entityTypeManager->expects($this->any())
      ->method('getstorage')
      ->willReturn($entityStorage);

    $helper = new HelperService($entityTypeManager);

    // Test different numbers for system-wide displays.
    $link = $helper->getLinkForGeoid('geonames_2911298', 'system-wide');
    $this->assertEquals('weather/Germany/Hamburg/Hamburg/1', $link);
    $link = $helper->getLinkForGeoid('geonames_2911298', 'system-wide', 1);
    $this->assertEquals('weather/Germany/Hamburg/Hamburg/1', $link);
    $link = $helper->getLinkForGeoid('geonames_2911298', 'system-wide', 7);
    $this->assertEquals('weather/Germany/Hamburg/Hamburg/7', $link,);

    $link = $helper->getLinkForGeoid('geonames_2911298', 'default');
    $this->assertEquals('weather/Germany/Hamburg/Hamburg', $link);
    $link = $helper->getLinkForGeoid('geonames_2911298', 'user');
    $this->assertEquals('weather/Germany/Hamburg/Hamburg/u', $link);

    $link = $helper->getLinkForGeoid('geonames_2911298', 'api');
    $this->assertEquals(
      'https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=53.33&lon=10',
      $link
    );

    $link = $helper->getLinkForGeoid('geonames_2911298', 'yr.no');
    $this->assertEquals(
      'https://www.yr.no/place/Germany/Hamburg/Hamburg/',
      $link
    );
  }

}
